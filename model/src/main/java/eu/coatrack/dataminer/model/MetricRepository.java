package eu.coatrack.dataminer.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MetricRepository extends JpaRepository<MetricEntity, Long>, JpaSpecificationExecutor<MetricEntity> {

}
