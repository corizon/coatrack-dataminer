package eu.coatrack.dataminer.rest.controller;

import eu.coatrack.dataminer.api.MetricDTO;
import eu.coatrack.dataminer.api.MetricService;
import eu.coatrack.dataminer.model.MetricEntity;
import eu.coatrack.dataminer.model.MetricRepository;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;

import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import static org.modelmapper.convention.MatchingStrategies.STRICT;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

@RestController
@RequestMapping(value = "/dataminer/api")
@Component
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class MetricsRestController implements MetricService, InitializingBean {

    private static final Logger log = LoggerFactory.getLogger(MetricsRestController.class);

    @Autowired(required = true)
    private MetricRepository roomTypeRepository;

    private ModelMapper modelMapper;

    @Override
    public void afterPropertiesSet() throws Exception {

        modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(STRICT);
        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
    }

    @GetMapping(value = "/metrices/{id}", produces = "application/json")
    @Override
    public MetricDTO findById(@PathVariable("id") Long id) {
        return toDTO(roomTypeRepository.findById(id).get());
    }

    @GetMapping(value = "/metrices/{id}.csv", produces = "text/csv")
    public void findByIdCSV(@PathVariable("id") Long id, HttpServletResponse response) throws IOException {

        MetricDTO metricDTO = toDTO(roomTypeRepository.findById(id).get());
        ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);

        csvWriter.write(metricDTO);

        csvWriter.close();
    }

    @GetMapping(value = "/metrices.csv")
    public void findAllCSV(HttpServletResponse response) throws IOException {

        List<MetricEntity> metrices = roomTypeRepository.findAll();
        ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);

        List<String> names = new ArrayList();
        Field[] allFields = MetricDTO.class.getDeclaredFields();
        for (Field field : allFields) {
            names.add(field.getName().toString());

        }
        String[] header = names.toArray(new String[names.size()]);

        csvWriter.writeHeader(header);
        for (MetricEntity item : metrices) {
            MetricDTO metricDTO = toDTO(item);
            csvWriter.write(metricDTO, header);
        }

        csvWriter.close();
    }

    @GetMapping(value = "/metrices", produces = "application/json")
    @Override
    public List<MetricDTO> findAll(Pageable pageable) {

        List<MetricDTO> dtos = roomTypeRepository.findAll()
                .stream()
                .map(ent -> toDTO(ent))
                .collect(Collectors.toList());
        return dtos;
    }

    MetricDTO toDTO(MetricEntity entity) {
        return modelMapper.map(entity, MetricDTO.class
        );
    }

    MetricEntity toEntity(MetricDTO dto) {
        return modelMapper.map(dto, MetricEntity.class
        );
    }
}
