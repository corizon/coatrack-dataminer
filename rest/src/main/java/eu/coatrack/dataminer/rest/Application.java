package eu.coatrack.dataminer.rest;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = {"nl.bebr.xdat.travel.roomtypes.model"})
@ComponentScan(basePackages = {"nl.bebr.xdat.travel.roomtypes.rest"})
@EntityScan(basePackages = {
    "nl.bebr.xdat.travel.roomtypes.model"
})
public class Application {

    public static void main(String[] args) {
        new SpringApplicationBuilder(Application.class).web(WebApplicationType.SERVLET).run(args);//.close();
    }

}
