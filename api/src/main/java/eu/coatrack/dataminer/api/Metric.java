package eu.coatrack.dataminer.api;

public interface Metric {

    Long getId();

    String getPath();

    
}
