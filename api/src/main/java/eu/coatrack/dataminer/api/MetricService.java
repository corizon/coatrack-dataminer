package eu.coatrack.dataminer.api;

import java.util.List;
import org.springframework.data.domain.Pageable;

public interface MetricService {

    MetricDTO findById(Long id);

    List<MetricDTO> findAll(Pageable pageable);

}
